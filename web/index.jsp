<%-- 
    Document   : index
    Created on : 28-ago-2018, 22:56:29
    Author     : miguel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Java web</title>
        <link href="css/css.css" rel="stylesheet">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
    </head>
    <body id="indexbd">
        <div class="container" style="margin-top:70px;">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 text-center">
                    <h1 id="pl">Bienvenido al gestor de usuarios</h1>
                    <hr>
                    <p id="pl">Esta pagina sirve para gestionar los usuarios que hay dento de la base de datos</p>
                    <p id="pl">Para poder acceder a esta pagina debe tener registrado un usuario</p>
                    <a href="Login.jsp" class="btn btn-default">Iniciar Sesion</a>
                    <hr>
                    <p id="pl" style="margin-top: 10px;">Presione iniciar secion para cargar el login</p>
                </div>
            </div>
        </div>
    </body>
</html>
