<%-- 
    Document   : Login
    Created on : 28-ago-2018, 22:56:55
    Author     : miguel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link href="css/css.css" rel="stylesheet">
    </head>
    <body id="BodyLogin">
        <div class="sidenav">
         <div class="login-main-text">
            <h2>Gestor de Usuarios<br> Login Page</h2>
            <p>Inicia sesion para ingresar, eliminar y modificar usuarios.</p>
         </div>
      </div>
      <div class="main">
         <div class="col-md-6 col-sm-12">
            <div class="login-form">
               <form action="sesioncontroller" method="post">
                  <div class="form-group">
                     <label>Usuario</label>
                     <input type="text" class="form-control" placeholder="Usuario" name="Nickname" required>
                  </div>
                  <div class="form-group">
                     <label>Contraseña</label>
                     <input type="password" class="form-control" placeholder="Contraseña" name="Contra" required>
                  </div>
                  <button type="submit" class="btn btn-black">Iniciar Sesion</button>
               </form>
            </div>
         </div>
      </div>
    </body>
</html>
