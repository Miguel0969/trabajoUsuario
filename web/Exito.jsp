<%-- 
    Document   : Exito
    Created on : 29-ago-2018, 15:16:42
    Author     : miguel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/css.css" rel="stylesheet">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <SCRIPT languaje="JavaScript">
            function pulsar() {
            alert("No agregado");
            }
        </SCRIPT>
        <title>Opciones</title>
    </head>
    <body id="BodyLogin">
        <h1 id="pl">Menu de Opciones</h1>
        <hr>
        <p style="color:red;" id="pl">La operacion se ha realizado correctamente</p>
        <div class="container"> 
            <div class="row">
                <div class="col-lg-6" style="margin-left: auto; margin-right: auto;">
                    <form action="RegistrarUsuario.jsp" style="display: inline-block;"><button type="submit" class="btn btn-outline-success" style="width: 170px; height: 350px;">Registrar Usuario</button></form>
                    <button type="button" class="btn btn-outline-danger" style="width: 170px; height: 350px;" onclick="pulsar()">Eliminar Usuario</button>
                    <button type="button" class="btn btn-outline-info" style="width: 170px; height: 350px;" onclick="pulsar()">Modificar Usuario</button>	
                </div>
            </div>
        </div>
        
        <p id="pl">Elija una opcion</p>
        <hr>
    </body>
</html>
