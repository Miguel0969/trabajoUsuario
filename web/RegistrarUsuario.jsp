<%-- 
    Document   : RegistrarUsuario
    Created on : 29-ago-2018, 14:08:11
    Author     : miguel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/css.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <title>Registrar</title>
    </head>
    <body>
        <div class="sidenav">
         <div class="login-main-text">
            <h2>Registrar<br> Login Page</h2>
            <p>Coloque los datos pedidos en pantalla para poder registrar el usuarios.</p>
         </div>
      </div>
      <div class="main">
         <div class="col-md-6 col-sm-12">
            <div class="login-form">
               <form action="registrarcontroller" method="get">
                  <div class="form-group">
                      <label>Nombre</label>
                     <input type="text" class="form-control" placeholder="Nombre" name="nom" required>
                  </div>
                   <div class="form-group">
                     <label>Apellido</label>
                     <input type="text" class="form-control" placeholder="Apellido" name="ape" required>
                  </div>
                  <div class="form-group">
                     <label>Usuario</label>
                     <input type="text" class="form-control" placeholder="Usuario" name="nick" required>
                  </div>
                   <div class="form-group">
                     <label>Contraseña</label>
                     <input type="password" class="form-control" placeholder="Contraseña" name="pass" required>
                  </div>
                  <button type="submit" class="btn btn-black">Registrar</button>
               </form>
            </div>
         </div>
      </div>
        
        
    </body>
</html>
