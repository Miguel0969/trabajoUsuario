
package Dao;

import Entidad.Usuario;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RegistrarDao {
    
    private DbConeccion con;

    public RegistrarDao(DbConeccion con) {
        this.con = con;
    }
    
    public boolean registrar(Usuario usuario){
        PreparedStatement registrarU;
        
        try{
            registrarU = con.getConnection().prepareStatement(
                    "INSERT INTO usuario (Nick,Contraseña, Nombre, Apellido)"
                            + "VALUES (?,?,?,?)");
            registrarU.setString(1, usuario.getNick());
            registrarU.setString(2, usuario.getContraseña());
            registrarU.setString(3, usuario.getNombre());
            registrarU.setString(4, usuario.getApellido());
            
            registrarU.executeUpdate();
            return true;
        } catch (SQLException e){
            System.out.println("Error: "+e);
            return false;
        }
    
    
    
}
}
