
package Dao;

import Entidad.Usuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class SesionDao {
    
    private DbConeccion con;

    public SesionDao(DbConeccion con){
        this.con = con;
    }
    
    public Usuario Sesion(String nick, String contra) {

        try {
            PreparedStatement preparedStatement = con.getConnection().prepareStatement("select * from usuario where Nick=? and Contraseña = ?");
            
            preparedStatement.setString(1, nick);
            preparedStatement.setString(2, contra);
            ResultSet rs = preparedStatement.executeQuery();
            Usuario us = new Usuario(0);
            while (rs.next()) {
                us.setIdUsuario(rs.getInt("IdUsuario"));
                us.setNick(rs.getString("Nick"));
                us.setContraseña(rs.getString("Contraseña"));
            }
            return us;
        } catch (SQLException e) {
            System.out.println("Error en el login dao: " + e.getMessage());
            return null;
        }
    } 
    
    
}
