
package Entidad;


public class Usuario {
    private int IdUsuario;
    private String nick;
    private String Contraseña;
    private String Nombre;
    private String Apellido;

    public Usuario() {
    }

    public Usuario(int IdUsuario) {
        this.IdUsuario = IdUsuario;
    }

    public Usuario(String nick, String Contraseña) {
        this.nick = nick;
        this.Contraseña = Contraseña;
    }

    public int getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(int IdUsuario) {
        this.IdUsuario = IdUsuario;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getContraseña() {
        return Contraseña;
    }

    public void setContraseña(String Contraseña) {
        this.Contraseña = Contraseña;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    @Override
    public String toString() {
        return "Usuario{" + "IdUsuario=" + IdUsuario + ", nick=" + nick + ", Contrase\u00f1a=" + Contraseña + ", Nombre=" + Nombre + ", Apellido=" + Apellido + '}';
    }
}
