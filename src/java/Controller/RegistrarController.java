/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.DbConeccion;
import Dao.RegistrarDao;
import Entidad.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author miguel
 */
public class RegistrarController extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        HttpSession session = request.getSession();
        DbConeccion con = new DbConeccion();
        RegistrarDao regDao = new RegistrarDao(con);
        Usuario usu = new Usuario();
        
        String nombre = request.getParameter("nom");
        String apellido = request.getParameter("ape");
        String nick = request.getParameter("nick");
        String pass = request.getParameter("pass");

        
        if(session.getAttribute("usuario") == null) {
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }else{ 
            usu.setNombre(nombre);
            usu.setApellido(apellido);
            usu.setNick(nick);
            usu.setContraseña(pass);
            
            regDao.registrar(usu);
            con.disconnect();
            request.getRequestDispatcher("/Exito.jsp").forward(request, response);
        } 
        
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
