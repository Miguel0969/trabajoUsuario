/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.DbConeccion;
import Dao.SesionDao;
import Entidad.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author miguel
 */
public class SesionController extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       
    }

  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        HttpSession session = request.getSession();
        Usuario usu= (Usuario)session.getAttribute("usuario");

        if (usu == null) {
            request.getRequestDispatcher("/Login.jsp").forward(request, response);
        }
        
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        HttpSession session = request.getSession();
        session.setAttribute("usuario", null);
        
        String nick=request.getParameter("Nickname");
        String pass=request.getParameter("Contra");
        DbConeccion con = new DbConeccion();
        SesionDao sesionDao = new SesionDao(con);
        Usuario usuario = sesionDao.Sesion(nick, pass);
        con.disconnect();
        if(usuario.getIdUsuario()>0){
            Usuario usu= new Usuario(nick,pass);
            session.setAttribute("usuario", usu);
            request.getRequestDispatcher("/MenuOpciones.jsp").forward(request, response);
            
        }else{         
            request.getRequestDispatcher("/Login.jsp").forward(request, response);
     
        }  
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
